package com.utn.notify;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.widget.Toast;
import com.utn.notify.controller.AppController;
import com.utn.notify.fragments.ExtendedFragment;
import com.utn.notify.fragments.groups.GroupListFragment;
import com.utn.notify.fragments.login.LoginDialogFragment;
import com.utn.notify.fragments.notifications.LoadPublicNotificationsRecipe;
import com.utn.notify.fragments.notifications.NotificationListFragment;
import com.utn.notify.model.Group;
import com.utn.notify.controller.OnSwapFragment;

import java.util.HashSet;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private static final int PUBLIC_NOTIFICATIONS_FRAGMENT = 0;
    private static final int RIGHT_FRAGMENT = 1;

    private ExtendedFragment leftFragment;
    private ExtendedFragment rightFragment = makeLoginFragment();

    private String leftFragmentDefaultName;
    private String rightFragmentDefaultName;

    private FragmentManager fragmentManager;
    private boolean loadingRightFragment;
    private HashSet<Class> rightFragmentList = new HashSet<Class>();

    private AppController appController;

    ViewPagerAdapter(FragmentManager fragmentManager, Context context) {
        super(fragmentManager);
        this.fragmentManager = fragmentManager;

        appController = AppController.getInstance();

        leftFragmentDefaultName = context.getResources().getString(R.string.tab_left_starting_title);
        rightFragmentDefaultName = context.getResources().getString(R.string.tab_right_starting_title);
        rightFragmentList.add(LoginDialogFragment.class);
        rightFragmentList.add(GroupListFragment.class);
        rightFragmentList.add(NotificationListFragment.class);
    }

    @Override
    public Fragment getItem(int position) {

        switch(position) {
            case PUBLIC_NOTIFICATIONS_FRAGMENT:
                NotificationListFragment leftFragment = new NotificationListFragment();
                leftFragment.setName(leftFragmentDefaultName);
                leftFragment.setLoadNotificationsRecipe(new LoadPublicNotificationsRecipe());
                return leftFragment;

            case RIGHT_FRAGMENT: {
                boolean isUserLoggedIn = AppController.getInstance().isUserLogged();

                if (isUserLoggedIn) {
                    if (!rightFragment.getClass().equals(NotificationListFragment.class)) {
                        rightFragment = makeGroupFragment();
                    }
                    return rightFragment;
                } else {
                    rightFragment = makeLoginFragment();
                    return rightFragment;
                }
            }

            default:
                throw new IllegalArgumentException("No fragment with " + position + " is specified.");
        }
    }

    private ExtendedFragment makeLoginFragment() {
        return LoginDialogFragment.newInstance(new OnSwapFragment() {
            @Override
            public void swapForward(Object param) {
                replace(rightFragment, makeGroupFragment());
            }
        });
    }

    private ExtendedFragment makeGroupFragment() {
        return GroupListFragment.newInstance(new OnSwapFragment() {
            @Override
            public void swapForward(Object param) {
                Group group = (Group)param;
                replace(rightFragment, NotificationListFragment.newInstance(group));
            }
        });
    }

    private void replace(final ExtendedFragment oldFragment, final ExtendedFragment newFragment) {
        appController.getActivity()
        .runOnUiThread(new Runnable() {
            @Override
            public void run() {
                fragmentManager.beginTransaction().remove(oldFragment).commit();
                rightFragment = newFragment;
                loadingRightFragment = true;
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemPosition(Object object) {

        if (rightFragmentList.contains(object.getClass())) {
            if (object instanceof NotificationListFragment) {
                NotificationListFragment fragment = (NotificationListFragment)object;
                if (fragment.getName().equals(leftFragmentDefaultName)) {
                    return PUBLIC_NOTIFICATIONS_FRAGMENT;
                }
            }

            if (loadingRightFragment) {
                loadingRightFragment = false;
                return POSITION_NONE;
            }
            return RIGHT_FRAGMENT;
        }
        return POSITION_UNCHANGED;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                if (leftFragment == null) return leftFragmentDefaultName;
                return leftFragment.getName();
            case 1:
                if (rightFragment == null) return rightFragmentDefaultName;
                return rightFragment.getName();
        }
        throw new IllegalArgumentException("No fragment with " + position + " is specified.");
    }

    @Override
    public int getCount() {
        return 2;
    }


    public boolean onBackScreen(int currentItem) {
        ExtendedFragment item = (ExtendedFragment)getItem(currentItem);

        if (item instanceof NotificationListFragment &&
            item.getName().equals(leftFragmentDefaultName)) {
            return false;
        }
        if (item instanceof LoginDialogFragment) return false;

        if (item instanceof GroupListFragment) {
            String msg = appController.getActivity().getString(R.string.group_fragment_msg_logged_out);
            appController.showToast(msg, Toast.LENGTH_SHORT);
            appController.logout();
            replace(rightFragment, makeLoginFragment());
        } else {
            replace(rightFragment, makeGroupFragment());
        }

        return true;
    }
}
