package com.utn.notify.fragments.login;

import android.widget.Toast;
import com.utn.notify.controller.AppController;
import com.utn.notify.controller.OnLoginListener;
import com.utn.notify.controller.OnSwapFragment;
import com.utn.notify.model.User;
import com.utn.notify.provider.LoginFailed;
import com.utn.notify.provider.OnLoginRequestResponse;
import com.utn.notify.R;

public class OnLoginButtonPressed implements OnLoginListener {
    private OnSwapFragment onSwap;
    public OnLoginButtonPressed(OnSwapFragment onSwap) {
        this.onSwap = onSwap;
    }

    @Override
    public void onLogin(String email, String password) {
        final AppController controller = AppController.getInstance();

        controller.login(email, password, new OnLoginRequestResponse() {
            @Override
            public void onSuccess(User user) {
                onSwap.swapForward(new Object());
            }

            @Override
            public void onFailure(int cause) {
                String msg = "";
                if (cause == LoginFailed.INVALID_LOGIN_DATA) {
                    msg = controller.getActivity().getString(R.string.login_fragment_msg_login_invalid);
                } else {
                    msg = controller.getActivity().getString(R.string.appcontroller_on_connection_failed);
                }
                controller.showToast(msg, Toast.LENGTH_SHORT);
            }
        });
    }

    @Override
    public void onDismissed() {
    }
}
