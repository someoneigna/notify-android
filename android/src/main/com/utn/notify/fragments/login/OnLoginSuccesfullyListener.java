package com.utn.notify.fragments.login;

import com.utn.notify.model.User;
import com.utn.notify.provider.OnLoginRequestResponse;

public class OnLoginSuccesfullyListener implements OnLoginRequestResponse {
    @Override
    public void onSuccess(User user) {
    }

    @Override
    public void onFailure(int cause) {
    }
}
