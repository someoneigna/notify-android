package com.utn.notify.fragments.login;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.utn.notify.fragments.ExtendedFragment;
import com.utn.notify.controller.AppController;
import com.utn.notify.controller.OnLoginListener;
import com.utn.notify.controller.OnSwapFragment;
import com.utn.notify.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;

public class LoginDialogFragment extends ExtendedFragment {
    private static final String TAG = "LoginDialogFragment";
    private Context context;

    private OnLoginListener onLoginListener = new OnLoginListener() {
        @Override
        public void onLogin(String email, String password) {
            throw new IllegalStateException("This point should not be reached.");
            //AppController.getInstance().showToast(TAG + ": " + "Email:" + email + " PASS:" + password, Toast.LENGTH_SHORT);
        }
        @Override
        public void onDismissed() {
        }
    };

    private AppCompatButton loginButton;
    private AppCompatEditText txtEmail, txtPassword;

    private Runnable onDetachCallback = new Runnable() {
        @Override
        public void run() {
        }
    };
    private View view;

    public LoginDialogFragment() {
        setName(AppController.getInstance().getActivity().getString(R.string.tab_right_starting_title));
    }

    public String getName() {
        return name;
    }

    public static ExtendedFragment newInstance(final OnSwapFragment onSwap) {
        ExtendedFragment fragment;

        LoginDialogFragment loginDialogFragment = new LoginDialogFragment();
        loginDialogFragment.setOnLoginButtonPressed(new OnLoginButtonPressed(onSwap));
        fragment = loginDialogFragment;

        return fragment;
    }

    public void setOnDetachCallback(Runnable callback) {
        onDetachCallback = callback;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.login_fragment, container, false);
        this.context = view.getContext();

        txtEmail = (AppCompatEditText)view.findViewById(R.id.input_email);
        txtEmail.requestFocus();

        txtPassword = (AppCompatEditText)view.findViewById(R.id.input_password);

        loginButton = (AppCompatButton)view.findViewById(R.id.btn_login);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               String hashedPassword = txtPassword.getText().toString();
               //hashedPassword = hash(txtPassword.getText().toString());
               onLoginListener.onLogin(txtEmail.getText().toString(), hashedPassword);
            }
        });

        return view;
    }

    private String hash(String text) {
        try {
            MessageDigest sha = MessageDigest.getInstance("SHA-256");
            String hash = sha.digest(text.getBytes()).toString();
            return hash;
        } catch (NoSuchAlgorithmException e) {
            Logger.getLogger(TAG).severe("SHA256 hashing is not supported.");
            return hash128(text);
        }
    }

    private String hash128(String text) {
        try {
            MessageDigest sha = MessageDigest.getInstance("SHA1");
            String hash = sha.digest(text.getBytes()).toString();
            return hash;
        } catch (NoSuchAlgorithmException e) {
            Logger.getLogger(TAG).severe("SHA1 hashing is not supported.");
            return text;
        }
    }

    @Override
    public void onDetach() {
        onDetachCallback.run();
        super.onDetach();
    }

    public void setOnLoginButtonPressed(OnLoginListener listener) {
       onLoginListener = listener;
    }
}
