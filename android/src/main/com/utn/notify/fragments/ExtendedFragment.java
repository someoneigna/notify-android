package com.utn.notify.fragments;

import android.support.v4.app.Fragment;

public class ExtendedFragment extends Fragment{
    protected String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
