package com.utn.notify.fragments.notifications;

import com.utn.notify.controller.AppController;
import com.utn.notify.model.Notification;

import java.util.List;

public class LoadPublicNotificationsRecipe implements  LoadNotificationsRecipe {

    public LoadPublicNotificationsRecipe() {
    }

    @Override
    public void load(List<Notification> notifications, final Runnable notifyCallback) {
        final AppController appController = AppController.getInstance();
        appController.getPublicNotifications(notifications, new Runnable() {
            @Override
            public void run() {
                appController.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        notifyCallback.run();
                    }
                });
            }
        });
    }
}
