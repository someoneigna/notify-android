package com.utn.notify.fragments.notifications;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.utn.notify.controller.AppController;
import com.utn.notify.fragments.ExtendedFragment;
import com.utn.notify.model.Group;
import com.utn.notify.model.Notification;
import com.utn.notify.R;

import java.util.ArrayList;
import java.util.List;

public class NotificationListFragment extends ExtendedFragment {
    private View view;
    private Context context;
    private RecyclerView listView;
    private List<Notification> notifications;
    private RecyclerView.Adapter listAdapter;
    private RecyclerView.LayoutManager listManager;

    private Runnable onDetachCallback = new Runnable() {
        @Override
        public void run() {
        }
    };
    private LoadNotificationsRecipe loadNotifications;

    public NotificationListFragment() {
        setName(AppController.getInstance().getActivity().getString(R.string.tab_left_starting_title));
    }

    public void setLoadNotificationsRecipe(LoadNotificationsRecipe recipe) {
        loadNotifications = recipe;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.group_list_fragment, container, false);
        this.context = view.getContext();

        if (notifications == null) {
            notifications = new ArrayList<Notification>();
        }

        listAdapter = new NotificationListAdapter(notifications);
        listView = (RecyclerView)view.findViewById(R.id.cardList);
        listView.setAdapter(listAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listManager = linearLayoutManager;
        listView.setLayoutManager(listManager);

        loadNotifications.load(notifications, new Runnable() {
            @Override
            public void run() {
                AppController.getInstance().getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listAdapter.notifyDataSetChanged();
                    }
                });
            }
        });

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onDetachCallback.run();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        onDetachCallback.run();
    }

    public static ExtendedFragment newInstance(Group group) {
        NotificationListFragment fragment = new NotificationListFragment();
        fragment.setLoadNotificationsRecipe(new LoadGroupNotificationsRecipe(group));
        fragment.setName(group.getName());
        return fragment;
    }
}
