package com.utn.notify.fragments.notifications;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.utn.notify.model.Notification;
import com.utn.notify.R;

import java.util.List;

public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.MyViewHolder> {
    private List<Notification> notificationList;
    private java.text.SimpleDateFormat hoursSinceFormat = new java.text.SimpleDateFormat("h");

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, content;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.txtNotificationTitle);
            content = (TextView) view.findViewById(R.id.txtNotificationContent);
        }
    }

    public NotificationListAdapter(List<Notification> notificationsList) {
        this.notificationList = notificationsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_list_item, parent, false);
        final MyViewHolder viewHolder = new MyViewHolder(itemView);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Notification notification = notificationList.get(position);
        holder.title.setText(notification.getName());
        holder.content.setText(notification.getContent());
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }
}
