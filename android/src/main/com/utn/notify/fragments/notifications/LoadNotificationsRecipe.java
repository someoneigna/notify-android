package com.utn.notify.fragments.notifications;

import com.utn.notify.model.Notification;

import java.util.List;

public interface LoadNotificationsRecipe {
    void load(List<Notification> notifications, Runnable notifyCallback);
}
