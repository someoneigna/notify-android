package com.utn.notify.fragments.notifications;

import android.widget.Toast;
import com.utn.notify.controller.AppController;
import com.utn.notify.model.Group;
import com.utn.notify.model.Notification;
import com.utn.notify.provider.OnNotificationsRequestResponse;
import com.utn.notify.R;

import java.util.List;

public class LoadGroupNotificationsRecipe implements LoadNotificationsRecipe {
    private Group group;

    public LoadGroupNotificationsRecipe(Group group) {
        this.group = group;
    }

    @Override
    public void load(final List<Notification> notifications, final Runnable notifyCallback) {
        final AppController appController = AppController.getInstance();
        appController.getGroupNotifications(group, new OnNotificationsRequestResponse() {
            @Override
            public void onSuccess(List<Notification> notification) {
                if (notification.size() == 0) {
                     appController.showToast(appController.getActivity().getString(R.string.loadgroupnotificationsrecipe_msg_empty_group), Toast.LENGTH_SHORT);
                    return;
                }
                notifications.addAll(notification);
                notifyCallback.run();
            }

            @Override
            public void onFailure() {
                appController.showToast(appController.getActivity().getString(R.string.loadgroupnotificationsrecipe_msg_failure), Toast.LENGTH_SHORT);
            }
        });
    }
}
