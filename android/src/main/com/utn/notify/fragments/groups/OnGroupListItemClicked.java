package com.utn.notify.fragments.groups;

import com.utn.notify.model.Group;

public interface OnGroupListItemClicked {
    void onItemClicked(Group group);
}
