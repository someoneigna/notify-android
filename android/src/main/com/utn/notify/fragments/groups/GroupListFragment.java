package com.utn.notify.fragments.groups;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.utn.notify.controller.AppController;
import com.utn.notify.fragments.ExtendedFragment;
import com.utn.notify.controller.OnSwapFragment;
import com.utn.notify.model.Group;
import com.utn.notify.provider.OnGroupsRequestResponse;
import com.utn.notify.R;

import java.util.ArrayList;
import java.util.List;

public class GroupListFragment extends ExtendedFragment {
    private static final String TAG = "GroupListFragment";

    private AppController appController;
    private View view;
    private Context context;
    private RecyclerView listView;
    private List<Group> groups;
    private RecyclerView.Adapter listAdapter;
    private RecyclerView.LayoutManager listManager;
    private Runnable onDetachCallback = new Runnable() {
      @Override
      public void run() {
      }
    };

   private OnGroupListItemClicked onGroupItemClicked = new OnGroupListItemClicked() {
      @Override
      public void onItemClicked(Group group) {

      }
   };

   private Runnable onLoadAction = new Runnable() {
      @Override
      public void run() {
      }
   };

   public GroupListFragment() {
       appController = AppController.getInstance();
       String msg = appController.getActivity().getString(R.string.group_viewfragment_title);
       setName(msg);
   }

   public String getName() {
       return name;
   }

    public static ExtendedFragment newInstance(final OnSwapFragment onSwap) {
        GroupListFragment fragment = new GroupListFragment();
        fragment.setOnClickItem(new OnGroupListItemClicked() {
            @Override
            public void onItemClicked(Group group) {
                onSwap.swapForward(group);
            }
        });
        return fragment;
    }

   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      this.view = inflater.inflate(R.layout.group_list_fragment, container, false);
      this.context = view.getContext();

       view.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

           }
       });

      if (groups == null) {
          groups = new ArrayList<Group>();
          appController.getGroups(new OnGroupsRequestResponse() {
              @Override
              public void OnSuccess(List<Group> groups) {
                  if (groups.size() == 0) {
                      appController.showToast(appController.getActivity().getString(R.string.group_fragment_msg_nogroup_available), Toast.LENGTH_SHORT);
                  }
                  setGroups(groups);
              }

              @Override
              public void onFailure() {
                    appController.showToast(appController.getActivity().getString(R.string.group_fragment_msg_server_unreachable), Toast.LENGTH_SHORT);
              }
          });
      }

      listAdapter = new GroupListAdapter(groups, onGroupItemClicked);
      listView = (RecyclerView)view.findViewById(R.id.cardList);
      listView.setAdapter(listAdapter);

      LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
      linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
      listManager = linearLayoutManager;
      listView.setLayoutManager(listManager);

      onLoadAction.run();
      return view;
   }

   public void setOnClickItem(OnGroupListItemClicked listener) {
      this.onGroupItemClicked = listener;
   }

   @Override
   public void onDetach() {
      super.onDetach();
      onDetachCallback.run();
   }

   @Override
   public void onDestroy() {
      super.onDestroy();
      onDetachCallback.run();
   }

   public void setGroups(List<Group> groups) {
      this.groups.addAll(groups);

      getActivity().runOnUiThread(new Runnable() {
         @Override
         public void run() {
           listAdapter.notifyDataSetChanged();
         }
      });
   }
}
