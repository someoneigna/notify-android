package com.utn.notify.fragments.groups;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.utn.notify.model.Group;
import com.utn.notify.R;

import java.util.List;

public class GroupListAdapter extends RecyclerView.Adapter<GroupListAdapter.MyViewHolder> {

    private OnGroupListItemClicked onGroupItemClicked;
    private List<Group> groups;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.txtGroupTitle);
        }
    }

    public GroupListAdapter(List<Group> groups, OnGroupListItemClicked onGroupItemClicked) {
        this.groups = groups;
        this.onGroupItemClicked = onGroupItemClicked;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.group_list_item, parent, false);
        final MyViewHolder viewHolder = new MyViewHolder(itemView);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onGroupItemClicked.onItemClicked(groups.get(viewHolder.getLayoutPosition()));
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Group group = groups.get(position);
        if (holder.title != null) {
            holder.title.setText(group.getName());
        }
    }

    @Override
    public int getItemCount() {
        return groups.size();
    }
}
