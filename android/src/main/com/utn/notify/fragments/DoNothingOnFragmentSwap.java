package com.utn.notify.fragments;

import com.utn.notify.controller.OnSwapFragment;

public class DoNothingOnFragmentSwap implements OnSwapFragment {
    @Override
    public void swapForward(Object object) {
    }
}
