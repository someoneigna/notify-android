package com.utn.notify.model;

public class User {
	private String id;
	private String userType;
	private String username;

	public User() {}

	public String getId() {
		return id;
	}

	public String getUserType() {
		return userType;
	}

	public String getUsername() {
		return username;
	}
}
