package com.utn.notify.model;

import java.util.Date;

public class Group {
    private String name;
    private boolean active;
    private String id;
    private Date modificationDate;

    public Group() {}
    public Group(String name, boolean active, Date modificationDate) {
        this.name = name;
        this.active = active;
        this.modificationDate = modificationDate;
    }

    public void set(String name, boolean active) {
        this.name = name;
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getLastUpdated() {
        return modificationDate;
    }
}
