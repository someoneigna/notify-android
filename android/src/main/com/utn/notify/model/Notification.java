package com.utn.notify.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Notification implements Parcelable {
	private String id;
	private String name, content, author;
	private Date creationDate;
	protected Date modificationDate;
	
	public Notification() {}
	public Notification(String name, String content)
	{
		this.name = name;
		this.content = content;
		this.creationDate = new Date();
	}

	public static final Creator<Notification> CREATOR = new Creator<Notification>() {
		@Override
		public Notification createFromParcel(Parcel in) {
			return new Notification(in);
		}

		@Override
		public Notification[] newArray(int size) {
			return new Notification[size];
		}
	};

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Notification that = (Notification) o;
		if (!id.equals(that.getId())) return false;
		return true;
	}

	@Override
	public int hashCode() {
		int result = name != null ? name.hashCode() : 0;
		result = 31 * result + (content != null ? content.hashCode() : 0);
		result = 31 * result + (author != null ? author.hashCode() : 0);
		result = 31 * result + (id != null ? id.hashCode() : 0);
		return result;
	}

	public String getName() {
		return name;
	}

	public String getContent() {
		return content;
	}

	public String getAuthor() { return author; }

	public Date getCreationDate() {
		return creationDate;
	}
	
	public void setCreationDate(Date date) {
		creationDate = date;
	}

	public String getId() {
		return id;
	}
	
	public Date getModificationDate() {
		if (modificationDate == null) {
			modificationDate = creationDate;
		}
		return modificationDate;
	}

    public void set(Notification value) {
    	this.name = value.name;
		this.content = value.content;
		this.modificationDate = value.modificationDate;
    }

	@Override
	public int describeContents() {
		return 0;
	}

	private Notification(Parcel in) {
		id = in.readString();
		author = in.readString();
		name = in.readString();
		content = in.readString();
		creationDate = (Date)in.readSerializable();
		modificationDate = (Date)in.readSerializable();
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeString(id);
		parcel.writeString(author);
		parcel.writeString(name);
		parcel.writeString(content);
		parcel.writeSerializable(creationDate);
		parcel.writeSerializable(modificationDate);
	}
}
