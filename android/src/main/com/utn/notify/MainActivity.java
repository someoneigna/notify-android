package com.utn.notify;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import com.utn.notify.controller.AppController;

import java.util.Observable;
import java.util.Observer;

public class MainActivity extends AppCompatActivity implements Observer {
	private AppController appController;

	private ViewPager viewPager;
	private TabLayout tabLayout;
	private ViewPagerAdapter viewPagerAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		appController = new AppController(this);
		bindViewPager();
	}

	private void bindViewPager() {
		viewPager = (ViewPager)findViewById(R.id.view_pager);
		tabLayout = (TabLayout)findViewById(R.id.tab_layout);

		viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), getApplicationContext());

		viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
		viewPager.setAdapter(viewPagerAdapter);
		tabLayout.setupWithViewPager(viewPager);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void update(Observable observable, Object data) {
	}

	@Override
	public void onBackPressed() {
		if (!viewPagerAdapter.onBackScreen(viewPager.getCurrentItem())) {
			super.onBackPressed();
		}
	}
}
