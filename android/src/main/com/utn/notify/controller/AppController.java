package com.utn.notify.controller;

import android.app.Activity;
import android.widget.Toast;
import com.utn.notify.model.Notification;
import com.utn.notify.model.User;
import com.utn.notify.model.Group;
import com.utn.notify.R;
import com.utn.notify.provider.*;

import java.util.List;
import java.util.logging.Logger;

public class AppController {
    private static AppController instance;

    private Activity activity;
    private NotifyAPIProvider apiService;
    private Logger logger = Logger.getLogger("AppController");
    private User user;
    private boolean userLogged;

    public AppController(Activity activity) {
        this.activity = activity;
        apiService = new NotifyAPIProviderImpl();
        instance = this;
    }

    public static AppController getInstance() {
        if (instance == null) {
            throw new IllegalStateException("Invalid AppController state. AppController constructor not called.");
        }
        return instance;
    }

    public boolean isUserLogged() {
        return userLogged;
    }

    /**
     * Calls the API to retrieve the public notifications.
     * @param notifications The current notification list collection to be updated.
     * @param onFinishedUpdate The action to run when the process is completed.
     */
    public void getPublicNotifications(final List<Notification> notifications, final Runnable onFinishedUpdate) {
        apiService.getPublicNotifications(new OnNotificationsRequestResponse() {
            @Override
            public void onSuccess(List<Notification> values) {
                filterReceivedNotifications(values, notifications);
                onFinishedUpdate.run();
            }

            @Override
            public void onFailure() {
                String msg = AppController.getInstance().getActivity().getString(R.string.appcontroller_on_connection_failed);
                showToast(msg, Toast.LENGTH_SHORT);
            }
        });
    }

    /**
     * Updates modified notifications in place and adds insert the new ones.
     * @param received
     * @param current
     */
    private void filterReceivedNotifications(List<Notification> received, List<Notification> current) {
        for (int i = 0, size = received.size(); i < size; i++) {
            Notification newItem = received.get(i);
            if (current.size() > 0 &&
                current.contains(newItem)) {

                for (int index = 0; index < current.size(); index++) {
                    Notification currentItem = current.get(index);
                    if (currentItem.equals(newItem) &&
                       !currentItem.getModificationDate().equals(newItem.getModificationDate())) {
                        currentItem.set(newItem);
                    }
                }
            } else {
                current.add(newItem);
            }
        }
    }

    /**
     * Calls the API with the login parameters.
     * @param email User email.
     * @param password User password.
     * @param callback Function to be called when login operation is finished.
     */
    public void login(String email, String password, final OnLoginRequestResponse callback) {
        final OnLoginRequestResponse innerCallback = new OnLoginRequestResponse() {
            @Override
            public void onSuccess(User newUser) {
                user = newUser;
                userLogged = true;
                callback.onSuccess(user);
            }

            @Override
            public void onFailure(int cause) {
                callback.onFailure(cause);
            }
        };

        try {
            apiService.login(email, password, innerCallback);
        }
        catch (OnLoginFailedException e) {
            logger.info("User login failed with message:" + e.getMessage());
            userLogged = false;
        }
    }

    public void logout() {
        userLogged = false;
    }

    public void getGroups(OnGroupsRequestResponse callback) {
        apiService.getGroups(user.getId(), callback);
    }

    public void getGroupNotifications(Group group, OnNotificationsRequestResponse callback) {
        apiService.getGroupNotifications(group, callback);
    }

    public Activity getActivity() {
        return activity;
    }

    public void showToast(final String msg, final int length) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity, msg, length).show();
            }
        });
    }
}
