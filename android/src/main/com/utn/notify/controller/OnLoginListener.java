package com.utn.notify.controller;

public interface OnLoginListener {
    void onLogin(String email, String password);
    void onDismissed();
}
