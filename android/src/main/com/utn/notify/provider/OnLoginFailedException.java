package com.utn.notify.provider;

public class OnLoginFailedException extends Exception {
    public OnLoginFailedException(String message) {
        super(message);
    }
}
