package com.utn.notify.provider;

import android.util.Base64;
import com.utn.notify.model.Group;
import com.utn.notify.model.Notification;
import com.utn.notify.model.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import okhttp3.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NotifyAPIProviderImpl implements NotifyAPIProvider {
    private static final String TAG = "NotifyAPIProvider";
    private Logger logger = Logger.getLogger("NotifyAPIProviderImpl");
    private Gson gson;

    public static final String SOURCE = "http://10.0.3.2:5000";

    private static final String LoginURL =  "/user/login";
    private static final String GetPublicNotificationsURL = "/all";
    private static final String GetGroupNotificationURL = "/notification/group/";
    private static final String GetUserGroups = "/user/groups/";
    private static final String SERVICE_KEY = "f3b718b3-217d-4e98-8f63-b3b2b25f231d";
    private static final String PASSWORD = "n0t1fy4nd1201d";
    private static final String USERNAME = "notify-android";
    private static final String AuthenticationKey = SERVICE_KEY + ":" + PASSWORD;

    public NotifyAPIProviderImpl() {
        gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .serializeNulls()
                .create();
    }

    /**
     * Transforms parameters into JSON to send in the request to the API.
     * @param keys The name of the parameters.
     * @param values The value of the parameters.
     * @return A JSON valid string with the passed parameters.
     */
    private String toJson(String[] keys, Object[] values) {
        StringBuilder json = new StringBuilder();
        json.append("{");

        for (int i = 0; i < keys.length; i++) {
           String key = keys[i];
           Object value = values[i];
           String string = (values[i] instanceof String ? "\"" + value.toString() + "\"" : value.toString());
           json.append(String.format("\"%s\":%s", key, string));
           if (i + 1 < keys.length) {
               json.append(",");
           }
        }
        json.append("}");
        return json.toString();
    }

    /**
     * Calls the API with the login information. As the API is stateless it just returns
     * if login is granted.
     * @param email
     * @param password
     * @param callback
     * @throws OnLoginFailedException
     */
    @Override
    public void login(String email, String password, final OnLoginRequestResponse callback) throws OnLoginFailedException {
        logger.log(Level.INFO, TAG + ": Submitting Login request");

        String authenticationField = Base64.encodeToString(AuthenticationKey.getBytes(), Base64.DEFAULT);

        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(
                MediaType.parse("application/json"), toJson(new String[] {"email", "password"}, new Object[] { email, password})
        );

        Request request = new Request.Builder()
                .header("Content-Type", "application/json")
                .header("Authorization", "Basic " + authenticationField.trim())
                .post(body)
                .url((SOURCE + LoginURL).trim())
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                logger.log(Level.INFO, TAG + ": Failed to login " + e.getMessage());
                callback.onFailure(LoginFailed.NO_CONNECTION_TO_SERVER);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    User user = gson.fromJson(response.body().string(), User.class);
                    callback.onSuccess(user);
                } else {
                    callback.onFailure(LoginFailed.INVALID_LOGIN_DATA);
                }
            }
        });
    }

    /**
     * Calls the API to retrieve the groups the passed userId is subscribed to.
     * @param userId
     * @param callback
     */
    public void getGroups(String userId, final OnGroupsRequestResponse callback) {
        String authenticationField = Base64.encodeToString(AuthenticationKey.getBytes(), Base64.DEFAULT);

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .header("Content-Type", "application/json")
                .header("Authorization", "Basic " + authenticationField.trim())
                .url((SOURCE + GetUserGroups + userId).trim())
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                logger.log(Level.INFO, "Failed to request notifications: " + e.getMessage());
                callback.onFailure();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.code() != 200) {
                    callback.onFailure();
                    return;
                }

                String json = response.body().string();
                json = new String(json.getBytes(), Charset.forName("UTF-8"));
                GroupsResponse value = gson.fromJson(json, GroupsResponse.class);
                callback.OnSuccess(Arrays.asList(value.getSubscribedGroups()));
            }
        });
    }

    /**
     * Calls the API to retrieve the current public notifications.
     * @param callback
     */
    @Override
    public void getPublicNotifications(final OnNotificationsRequestResponse callback) {
        logger.log(Level.INFO, "Submitting request");

        String authenticationField = Base64.encodeToString(AuthenticationKey.getBytes(), Base64.DEFAULT);

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .header("Content-Type", "application/json")
                .header("Authorization", "Basic " + authenticationField.trim())
                .url((SOURCE + GetPublicNotificationsURL).trim())
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                logger.log(Level.INFO, "Failed to request notifications: " + e.getMessage());
                callback.onFailure();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String json = response.body().string();
                json = new String(json.getBytes(), Charset.forName("UTF-8"));
                List<Notification> values = Arrays.asList(gson.fromJson(json, Notification[].class));
                callback.onSuccess(values);
            }
        });
    }

    @Override
    public boolean updatesPending() {
        return false;
    }

    /**
     * Calls the API with the groupId to retrieve the group current notifications.
     * @param group
     * @param callback
     */
    @Override
    public void getGroupNotifications(Group group, final OnNotificationsRequestResponse callback) {
        final HashMap<String, String> headersConfiguration = new HashMap<String, String>();
        String authenticationField = Base64.encodeToString(AuthenticationKey.getBytes(), Base64.DEFAULT);

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .header("Content-Type", "application/json")
                .header("Authorization", "Basic " + authenticationField.trim())
                .url((SOURCE + GetGroupNotificationURL + group.getId()).trim())
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                logger.log(Level.INFO, "Failed to request notifications: " + e.getMessage());
                callback.onFailure();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.code() != 200) {
                    callback.onFailure();
                    return;
                }

                String json = response.body().string();
                json = new String(json.getBytes(), Charset.forName("UTF-8"));
                List<Notification> values = Arrays.asList(gson.fromJson(json, Notification[].class));
                callback.onSuccess(values);
            }
        });
    }
}
