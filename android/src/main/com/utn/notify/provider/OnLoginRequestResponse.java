package com.utn.notify.provider;

import com.utn.notify.model.User;

public interface OnLoginRequestResponse {
    void onSuccess(User user);
    void onFailure(int cause);
}
