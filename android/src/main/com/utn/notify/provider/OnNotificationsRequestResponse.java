package com.utn.notify.provider;

import com.utn.notify.model.Notification;

import java.util.List;

public interface OnNotificationsRequestResponse {
    void onSuccess(List<Notification> notification);
    void onFailure();
}
