package com.utn.notify.provider;

import com.utn.notify.model.Group;

public interface NotifyAPIProvider {
    void login(String username, String password, OnLoginRequestResponse callback) throws OnLoginFailedException;
    void getPublicNotifications(OnNotificationsRequestResponse callback);
    boolean updatesPending();
    void getGroupNotifications(Group group, OnNotificationsRequestResponse callback);
    void getGroups(String userId, OnGroupsRequestResponse callback);
}
