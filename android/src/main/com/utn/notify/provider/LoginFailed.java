package com.utn.notify.provider;

import android.net.Uri;

public class LoginFailed extends Exception {
    private Uri uri;
    public static final int INVALID_LOGIN_DATA = 0;
    public static final int NO_CONNECTION_TO_SERVER = 1;

    public LoginFailed(Uri uri, String message) {
        super(message);
        this.uri = uri;
    }

    public Uri getFailedUri() {
        return uri;
    }
}
