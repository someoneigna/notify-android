package com.utn.notify.provider;

import com.utn.notify.model.Group;

public class GroupsResponse {
    private Group[] owned_groups, subscribed_groups;

    public Group[] getOwnedGroups() {
        return owned_groups;
    }

    public Group[] getSubscribedGroups() {
        return subscribed_groups;
    }
}
