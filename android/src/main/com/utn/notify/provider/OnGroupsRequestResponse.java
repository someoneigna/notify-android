package com.utn.notify.provider;

import com.utn.notify.model.Group;

import java.util.List;

public interface OnGroupsRequestResponse {
    void OnSuccess(List<Group> groups);
    void onFailure();
}
